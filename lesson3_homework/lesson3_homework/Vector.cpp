#include"Vector.h"
#include<iostream>
#include<algorithm>

/*
This is the constructor function for the vector class.
*/
Vector::Vector(int n)
{
	//Allocates memory for the elements.
	this->_elements = new int[n];
	for (int i = 0; i < n; i++)
		_elements[i] = NULL;
}

/*
This is the copy constructor function for the vector class.
*/
Vector::Vector(const Vector& other)
{
	*this = other;
}


/*
This is the destructor function for the vector class.
*/
Vector::~Vector()
{
	//Frees all the memory allocated for the elements.
	delete[] _elements;
	_elements = nullptr;
}

/*
This function will return the size of the vector. The size is the number of elements.
Input: vector
Output: size - number of elements.
*/
int Vector::size()
{
	int current = this->_elements[0];
	int counter = 0;

	//Count the number of elements in the vector, if we reach NULL we reached the end of the legit elements.
	while (current != NULL)
		++counter;

	return counter;
}

/*
This function will return the max capacity of the current vector.
input: vector
output: int - the max capacity of the vector
*/
int Vector::capacity()
{
	//All bytewise size of the elements devided by the bytewise size of int will resualt in the max size of _elements.
	return(sizeof(this->_elements) / sizeof(int));
}

/*
This function will return the resize factor of the current vector.
input: vector
output: int - the resize factor of the vector.
*/
int Vector::resizeFactor()
{
	return this->_resizeFactor;
}

/*
This function will check if the current vector is empty.
input: vector
output: bool - empty or not
*/
bool Vector::empty()
{
	if (_elements[0] == NULL)
		return true;
	return false;
}

/*
This function will return the last element in the vector and delete it.
input: vector
output: int - the last element in the vector.
*/
int Vector::pop_back()
{
	int current = _elements[0];

	//If the vector is empty we return -9999 and print an error.
	if (this->empty())
	{
		std::cerr << "error: pop from empty vector";
		return -9999;
	}

	//The vector is full so we return the last element in the vector
	else if (this->size() == this->capacity())
		return this->_elements[this->size() - 1];
	
	for (int i = 0; i < this->size(); i++)
	{
		if (this->_elements[i] == NULL)
		{
			this->_elements[i - 1] = NULL;//Delete the poped element
			this->_size--;//Update the size.
			break;
		}
		current = this->_elements[i];
	}

	return current;
}

/*
This function will check if the vector is big enough.
If not, it will allocate more memory.
input: the size we want.
*/
void Vector::reserve(int n)
{
	int* new_elements;
	int i = 0, increment = 0;//as the task us to do, incase that capacity + factor isn't big enough.

	if (this->capacity() <= n)
	{
		//calculates the increment, if its not needed it will be one.
		increment = ((n - this->capacity()) / this->resizeFactor()) + 1;

		new_elements = new int[this->capacity() + (this->resizeFactor() * increment)];

		for (i = 0; i < (this->capacity() + (this->resizeFactor() * increment)); i++)
			new_elements[i] = NULL;

		//copy the old values from the old arr to the new arr.
		for (i = 0; i < this->size(); i++)
			new_elements[i] = this->_elements[i];

		//Update the info of the vector.
		this->_elements = new_elements;
		this->_capacity = this->capacity();

		delete[] _elements;
	}
}

void Vector::resize(int n)
{
	int* new_elements;
	int i = 0;

	if (this->capacity() >= n)
	{
		new_elements = new int[n];

		for (i = 0; i < n; i++)
			new_elements[i] = this->_elements[i];

		//Update the info of the vector.
		this->_elements = new_elements;
		this->_capacity = n;

		delete[] _elements;
	}

	else if (this->capacity() < n)
	{
		this->reserve(n);
	}
}

void Vector::assign(int val)
{
	//If there is no more room in the vector.
	if (this->size() == this->capacity())
		this->resize(this->capacity() + 1);

	this->_elements[this->size()] = val;
}

void Vector::resize(int n, const int& val)
{
	this->resize(n);
	for (int i = this->size(); i < this->capacity(); i++)
		this->_elements[i] = val;
}

Vector& Vector::operator=(const Vector& other)
{
	delete[] this->_elements;

	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;

	this->_elements = new int[this->_capacity];

	for (int i = 0; i < this->_capacity; i++)
		this->_elements[i] = other._elements[i];

	return *this;
}

int& Vector::operator[] (int n)
{
	if (n >= this->size())
	{
		std::cerr << "error: n exceeds the givin size.";
		return this->_elements[0];
	}
	return this->_elements[n];
}