#pragma once
#include<iostream>


//-------------There is no need to use the first in the "big three" (destructor) because were are no class members in the vector class.------------------------------------------------------
class Vector {
private:
	int* _elements;
	int _size;
	int _capacity;
	int _resizeFactor;

public:
	//Operators
	Vector& operator=(const Vector& other);
	int& operator[] (int v);

	//Constructor and Destructor
	Vector(int n);
	Vector(const Vector& other);
	~Vector();

	//Getters
	int size();
	int capacity();
	int resizeFactor();

	//Method
	bool empty();
	int pop_back();
	void reserve(int n);
	void resize(int n);
	void assign(int val);
	void resize(int n, const int& val);

};
