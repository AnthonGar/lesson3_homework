#include"Car.h"
#include<iostream>

Car::Car(std::string owner, double price, CarColor color, std::string model, std::string company)
{
	this->_carColor = CarColor::CarColor(color);
	this->_owner = owner;
	this->_price = price;
	this->_model = model;
	this->_company = company;
}

std::string Car::getOwner() const
{
	return this->_owner;
}

double Car::getPrice() const
{
	return this->_price;
}

CarColor Car::getColor() const
{
	return this->_carColor;
}

std::string Car::getModel()const
{
	return this->_model;
}

std::string Car::getCompany()const
{
	return this->_company;
}

void Car::print() const
{
	std::cout << "Model: ";
}

