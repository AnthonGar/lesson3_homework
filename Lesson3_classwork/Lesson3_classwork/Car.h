#pragma once
#include"CarColor.h"
#include<iostream>

class Car {
private:
	std::string _owner;
	double _price;
	CarColor _carColor;
	std::string _model;
	std::string _company;

public:
	//constructor
	Car(std::string owner, double price, CarColor carColor, std::string model, std::string company);

	//getters and setters
	std::string getOwner() const;
	double getPrice() const;
	CarColor getColor() const;
	std::string getModel()const;
	std::string getCompany() const;

	//Methods
	void print() const;
};
