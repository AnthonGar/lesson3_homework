#pragma once
#include<iostream>

class CarColor {
public:
	//Const Enum of colors for the car class
	const enum Color { RED, WHITE, GREEN, YELLOW, PINK, BLUE };

	//Constructors (Bobs :D )
	CarColor(const Color& color);
	CarColor();

	//Getters
	Color getColor() const;

	//Operators
	bool operator== (const CarColor& other) const;

private:
	//Members
	Color _CarColor;
};