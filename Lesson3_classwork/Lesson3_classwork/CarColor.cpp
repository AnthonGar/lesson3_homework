#include"CarColor.h"

CarColor::CarColor(const Color& color)
{
	this->_CarColor = color;
}

CarColor::CarColor()
{
	this->_CarColor = WHITE;
}

CarColor::Color CarColor::getColor() const
{
	return this->_CarColor;
}

bool CarColor::operator==(const CarColor & other) const
{
	return this->_CarColor == other._CarColor;
}
